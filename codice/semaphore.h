/// @file semaphore.h
/// @brief Contiene la definizioni di variabili e funzioni
///         specifiche per la gestione dei SEMAFORI.

#pragma once

#include <sys/sem.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/stat.h>
#include <errno.h>

// Temp
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>

union semun{
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

// Dichiarazione funzioni per la gestione dei semafori
/// @brief data la chiave, quanti semafori e una stringa per eventuali
///         errori, ritorna l'id del set di semafori
int sem(key_t, int, char*);

/// @brief La funzione blocca l'esecuzione finchè il semaforo all'id specificato
///         non è positivo. Ritorna EINTR se l'attesa è stata interrotta da un segnale.
int P(int);

/// @brief La funzione aggiunge 1 al semaforo specificato, eventualmente sbloccando
///         atri processi
int V(int);

/// @brief La funzione inizializza il semaforo del set specificato al valore
///         richiesto
void init(int, int, int);

// Dichiarazione globale nomi semafori
int acksemset;
int chessaccess;
int devices;