/// @file defines.h
/// @brief Contiene la definizioni di variabili
///         e funzioni specifiche del progetto.

#pragma once

#include <sys/types.h>

#include "err_exit.h"

#define ATTEMPTS 15
#define TIMEOUT 20
#define ERROR "\e[1;31m"
#define SUCCESS "\e[1;32m"
#define WARNING "\e[1;33m"
#define REQUEST "\e[1;36m"
#define DEFAULT "\e[0m"
#define SEMAPHORE_KEY 3456

#define ROWLEN 5*4
#define ROWN 5
#define FILECHARCOUNT ROWLEN*ROWN
#define ACKN 100
#define N_DEVICE 5

typedef struct {

    pid_t pid_sender;
    pid_t pid_receiver;
    int message_id;
    char message[256];
    int max_distance;

} Message;

typedef struct  {

    pid_t pid_sender;
    pid_t pid_receiver;
    int message_id;
    time_t timestamp;

} Acknowledgment;

struct queued_message {

    long mytype;
    Acknowledgment devices_acks[N_DEVICE];

};

union acklist_union {

    int message_id;
    Acknowledgment device_ack;

};
