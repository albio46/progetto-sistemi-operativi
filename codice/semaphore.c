/// @file semaphore.c
/// @brief Contiene l'implementazione delle funzioni
///         specifiche per la gestione dei SEMAFORI.

#include "err_exit.h"
#include "semaphore.h"
#include "defines.h"

int sem(key_t key, int n, char* msg){

    int semset = semget(key, n, IPC_CREAT | S_IRWXU | S_IRWXG | S_IRWXO);
    if(semset == -1){
        ErrExit(msg);
    }

    return semset;
}

void init(int semid, int n, int val){

    if(semctl(semid, n, SETVAL, val) == -1){
        ErrExit("[" ERROR "ERROR" DEFAULT "]: Semaphore init");
    }
    
}

int P(int semid){
    int result;

    // Attendo che sia 1 e, quando lo è, lo riduco a 0
    struct sembuf op[] = {{0, -1, 0}};

    result = semop(semid, op, 1);

    if(result == -1){
        if(errno == (EINTR | EIDRM | EINVAL)){
            return -1;
        } else {

            ErrExit("[" ERROR "ERROR" DEFAULT "]: Wait sem");
        }
    }

    return 0;
}

int V(int semid){
    int result;

    // Aggiungo 1 al semaforo, riportandolo a 0
    struct sembuf op[] = {{0, 1, 0}};
    
    result = semop(semid, op, 1);

    if(result == -1){
        if(errno == (EINTR | EIDRM | EINVAL)){
            return -1;
        } else {

            ErrExit("[" ERROR "ERROR" DEFAULT "]: Signal sem");
        }
    }

    return 0;
}