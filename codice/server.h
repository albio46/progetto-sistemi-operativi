/// @file server.h
/// @brief Contiene la definizioni di variabili e funzioni
///         specifiche per la gestione del server, oltre alle librerie

#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>


#include "defines.h"
#include "shared_memory.h"
#include "semaphore.h"
#include "err_exit.h"
#include "device.h"
#include "ack_manager.h"

// Flag per il ciclo del server
extern int flag;

/// @brief Funzione di terminazione del loop principale e, quindi, del programma
///         imposta la variabile globale flag a 0
void terminazione(int);

/// @brief Funzione di ricerca in array ordinato. Complessità logaritmica
///         richiede il message_id da cercare e ritorna la posizione in cui
///         ha trovato corrispondenza o -1 altrimenti
///         - thread safe -
int findAck(int message_id);

/// @brief Funzione che compila un array: 0 se il device alla posizione non 
///         ha ricevuto il messaggio, 1 altrimenti
///         - thread safe - 
int getReceivedarr(int, int*);
