/// @file device.h
/// @brief contiene la dichiarazione delle funzioni e delle costanti
///         utili all'implementazione del device, oltre alle librerie

#pragma once

#include "defines.h"
#include "shared_memory.h"
#include "server.h"

extern int flag;

struct msgList
{
    struct msgList *next;
    Message msg;
};

/// @brief Funzione di callback per la gestione del movimento. Sapendo che 
///         device l'ha chiamata, cerca il pid sulla scacchiera e lo sposta
///         alle nuove coordinate
int movementhandler(int, int*, pid_t);

/// @brief Funzione che ricerca annulla la posizione precedente le cui coordinate
///         sono al terzo argomento e inserisce il pid specificato alle coordinate
///         specificate al primo argomento
///         - thread safe -
int movement(int*, int*, pid_t);

/// @brief Funzione per mandare un messaggo ad un device pid
///         gestisce autonomamente variabili e file descriptor
void sendMessage(pid_t, Message*);

/// @brief Funzione che aggiunge un anello in fondo alla lista dei messaggi in possesso
///         di un device. Soervono la testa della lista e il messaggio da aggiungere
void addLast(struct msgList*, Message*);

/// @brief Funzione che spedisce un messaggio a tutti i vicini che non
///         l'hanno ricevuto. dato il messaggio, l'array dei vicini a cui è possibile
///         mandare il messaggio e l'array dei device che non l'hanno ricevuto, ritorna 
///         a quanti device l'ha mandato. 0 se non può mandare messaggi.
int sendCheck(Message*, int*, int*);

/// @brief Funzione che rimuove l'elemento puntato in prima posizione
///         va a cercarlo percorrendo la lista, quindi in seconda posizione va la testa
void removeMsg(struct msgList*, struct msgList*);

/// @brief Riempie un array di 5 posizioni di interi dove 1 è un vicino a cui può
///         mandare il messaggio. Richiede il raggio e la posizione centrale da cui controllare
///         la ricerca è radiale.
int getNeighbours(int, int[2], int*);

/// @brief Funzione per inserire un elemento nella tabella degli ack, in modo ordinato crescente
int insertElement(union acklist_union);

/// @brief La funzione "main" dei device
void device(int);

void terminazionedevice(int);