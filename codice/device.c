/// @file device.c
/// @brief contiene l'implementazione della funzione del singolo device

#include "device.h"
// TODO: Gestire la terminazione per timeout

void device(int n){

    // registrazione handler terminazione
    signal(SIGUSR2, terminazionedevice);

    // Buffer messagio ricevuto
    Message *message = (Message *)malloc(sizeof(Message));
    // Struttura temporanea acknowledgment
    union acklist_union *ack = (union acklist_union *)malloc(sizeof(union acklist_union));
    // Inizializzazione lista messaggi
    struct msgList *msglistHead = (struct msgList *)malloc(sizeof(struct msgList));
    msglistHead->next = NULL;
    struct msgList *index;
    int ridden, pid = devicespids[n-1], pos[2] = {0}, result;        
    struct sembuf v[] = {{(n+1 == N_DEVICE+1) ? (0) : (n+1), 1, 0}};
    struct sembuf p[] = {{n, -1, 0}};

    // Creo la stringa del path alla fifo del device
    char addr[20];
    sprintf(addr, "/tmp/dev_fifo.%d",pid);

    // Apertura fifo
    int fifo = mkfifo(addr, S_IRWXU);
    if (fifo == -1){
        ErrExit("Creazione fifo");
    }

    // Apertura fifo in lettura
    int fifofd = open(addr, O_RDONLY | O_NONBLOCK);
    if(fifofd == -1){
        ErrExit("FIFO open");
    }

    // inizio ciclo
    do{

        // Attendo il via dal dispositivo precedente
        result = semop(devices, p, 1);
        if(result == -1){
            if(errno == EINTR || errno == EIDRM || errno == EINVAL){
                break;
            } else {
                ErrExit("Device sem");
            }
        }

        /* ********************** */
        /*  Spedizione messaggi   */
        /* ********************** */
        index = msglistHead->next;
        struct msgList *next;

        // index->msg è il messaggio in questione
        while(index){

            // Mi salvo il puntatore al prossimo blocco, nel caso debba eliminare questo
            next = index->next;

            // Controllo necessità spedizione per ogni messaggio  
            int msg_pos = findAck(index->msg.message_id);

            // Attesa al semaforo interrotta
            if(msg_pos == -1){
                break;
            }

            if(msg_pos == -ACKN){

                // Il messaggio è già stato ricevuto da tutti ed eliminato dall'ack_manager
                // Elimino il messaggio dalla lista
                removeMsg(index, msglistHead);

            } else {

                // Se esiste controllo chi l'ha ricevuto
                int received[N_DEVICE] = {0};
                int sum = getReceivedarr(msg_pos, received);

                // C'è stato un problema con il semaforo
                if (sum == -1){

                    break;
                }

                if(sum == N_DEVICE){
                    // L'hanno già ricevuto tutti, elimino
                    removeMsg(index, msglistHead);
                } else {

                    // Cerco vicini a cui spedirlo
                    int neighbours[N_DEVICE] = {0};

                    if(getNeighbours(index->msg.max_distance, pos, neighbours) == -1){
                        break;
                    }

                    // Spedisco a chi devo
                    int sent = sendCheck(&(index->msg), neighbours, received);
                    if(sent == -1){
                        // TODO: Errore, ma non so bene cosa fare
                        printf("[" ERROR "ERROR" DEFAULT "]: Errore! c'è stato un problema a spedire il messaggio");
                    }

                    // Se l'ho spedito a qualcuno, lo elimino 
                    if(sent == 1){

                        removeMsg(index, msglistHead);
                    }

                }

            }

            // Scorro la lista
            index = next;

        }
        
        /* ********************** */
        /*   Ricezione messaggi   */
        /* ********************** */
        // Leggere tutti i nuovi messaggi nella coda
        while((ridden = read(fifofd, message, sizeof(Message))) != 0 ){

            // Rilevo l'errore sulla lettura e controllo se è solo perchè qualcuno sta scrivendo sulla fifo
            if (ridden == -1){

                if(errno != (EAGAIN | EWOULDBLOCK)){

                    ErrExit("[" ERROR "ERROR" DEFAULT "]: Read fifo");
                }

                break;

            } else {    // Se supera il controllo di errore

                // Aggiungo il messaggio alla lista
                addLast(msglistHead, message);

                // Riempio il messaggio di acknowledgment
                ack->device_ack.pid_sender = message->pid_sender;
                ack->device_ack.pid_receiver = pid;
                ack->device_ack.message_id = message->message_id;
                ack->device_ack.timestamp = time(NULL);

                // Cerco uno slot libero nella lista di ack
                int ackpos;
                if ((ackpos = findAck(ack->device_ack.message_id)) == -1){

                    // C'è stato un errore con il semaforo, devo uscire
                    break;
                } else if(ackpos == -ACKN){

                    // Non trovato, da aggiungere
                    ackpos = insertElement(*ack);
                    if (ackpos == -1){
                        break;
                    }
                }

                // Aggiungo il mio ack al message_id
                if(P(acksemset) == -1){
                    break;
                }
                acklist[ackpos][n].device_ack = ack->device_ack;
                if(V(acksemset) == -1){
                    break;
                }

            }
            
        }

        // Movimento
        if(movementhandler(n, pos, pid) == -1){
            break;
        }

        // Stampa finale, come da manuale
        printf("%d %d %d msgs: ", pid, pos[0], pos[1]);
        index = msglistHead->next;
        while(index != NULL){
            printf("%d ", index->msg.message_id);
            index = index->next;
        }
        printf("\n");

        // Libero il prossimo device
        result = semop(devices, v, 1);
        if(result == -1){
            if(errno == EINTR || errno == EIDRM || errno == EINVAL){
                break;
            } else {
                ErrExit("[" ERROR "ERROR" DEFAULT "]: Device sem");
            }
        }
      
    } while (flag);

// Debug
//printf("Device #%d ripulisce\n",n);
    // Pulizia del file descriptor della fifo e della fifo
    close(fifofd);
    unlink(addr);

    free(ack);
    free(message);

    // Pulisco la lista dei messaggi rimasti
    struct msgList *temp;
    index = msglistHead;
    do{
        temp = index;
        index = index->next;
        free(temp);
    } while(index != NULL);

}

int movement(int *ppos, int *npos, pid_t pid){

    // Attendo l'accesso alla chessboard
    if(P(chessaccess) == -1){
        return -1;
    }

    // Ripulisco la posizione precedente e scrivo quella nuova
    chessboard[ppos[0]][ppos[1]] = 0;
    chessboard[npos[0]][npos[1]] = pid;

    // Libero l'accesso
    if(V(chessaccess) == -1){
        return -1;
    }

    return 0;
}

int movementhandler(int n, int *ppos, pid_t pid){

    // Ottengo il carattere della posizione relativa a quale device sono
    int npos[2] = {0};
    npos[0] = atoi(&positionstring[(n-1)*4]);
    npos[1] = atoi(&positionstring[(n-1)*4+2]);

    // Attualizzo i cambiamenti sulla tavola
    if(movement(ppos, npos, pid) == -1){
        return -1;
    }

    // Aggiorno la posizione
    ppos[0] = npos[0];
    ppos[1] = npos[1];

    return 0;
}

void sendMessage(pid_t pid, Message *msg){

    // Costruisco il path alla fifo dell'altro device
    char pathneighbour[20];
    sprintf(pathneighbour, "/tmp/dev_fifo.%d", pid);

    // Apro la fifo
    int neighbourfifo = open(pathneighbour, O_WRONLY | O_NONBLOCK);
    if(neighbourfifo == -1){
        ErrExit("[" ERROR "ERROR" DEFAULT "]: Open neighbour fifo");
    }

    // Creo il nuovo messaggio, cambiando i pid
    Message new = *msg;
    new.pid_sender = new.pid_receiver;
    new.pid_receiver = pid;

    // Mando il messaggio
    int result = write(neighbourfifo, &new, sizeof(new));
    if(result == -1){        
        ErrExit("[" ERROR "ERROR" DEFAULT "]: Write on neighbour fifo");
    }
    close(neighbourfifo);
}

// Funzione per controllare se un messaggio deve essere inviato ai pid in range e a quali
int sendCheck(Message *message, int *neighbours, int *notreceived){
    // tab è la tabella in cui l'ack_manager registra gli ack
    
    // &~ dei device in range e dei device che hanno ricevuto e conta di quanti sono
    int res[N_DEVICE], sum = 0;
    for(int j = 0; j < N_DEVICE; j++){
        res[j] = neighbours[j] & ~notreceived[j];
        sum += res[j];
    }

    // Se non posso mandarlo a nessuno, esco
    if(sum == 0){
        return 0;
    }

    // Mando il messaggio al primo device che trovo
    for(int j = 0; j < N_DEVICE; j++){

        if(res[j] == 1){
            sendMessage(devicespids[j], message);
            return 1;
        }        
    }

    // Return di sicurezza
    return -1;

}

void removeMsg(struct msgList *toRemove, struct msgList *listHead){
    struct msgList *index;

    // Cerco il blocco precedente a quello da rimuovere
    index = listHead;
    while(index->next != toRemove){
        index = index->next;
    }

    // Riallaccio la catena
    index->next = toRemove->next;
    // Libero la memoria da liberare
    free(toRemove);
}

void addLast(struct msgList *head, Message *toadd){
    // Creo un nuovo anello
    struct msgList *element = (struct msgList *)malloc(sizeof(struct msgList));
    if(element == NULL){
        ErrExit("[" ERROR "ERROR" DEFAULT "]: New msg malloc");
    }

    // Riempio i campi dell'anello
    element->next = NULL;
    element->msg = *toadd;

    // sistemo i collegamenti
    struct msgList *index = head;
    while(index->next){
        index = index->next;
    }
    index->next = element;
}

// Ricerca vicini che soddisfino il range del messaggio
int getNeighbours(int radius, int pos[2], int *neighbours){
    
    // Cerco in TUTTA LA TAVOLA
    for(int i = 0; i < 9; i++){
        for(int j = 0; j < 9; j++){

            if(P(chessaccess) == -1){
                return -1;
            }

            // Controllo se è un buco nell'acqua
            if(chessboard[i][j] != 0){

                // Ho trovato qualcuno, ma potrei essere io
                if(i != pos[0] || j != pos[1]){

                    // Calcolo se è entro la distanza radiale
                    int distance = (int)sqrt(pow((pos[0] - i), 2) + pow((pos[1] - j), 2));

                    // Controllo se soddisfa il raggio dato dal messaggio
                    if(distance <= radius){

                        // Cerco di capire quale device è, per segnarmelo al posto giusto
                        for(int k = 0; k < N_DEVICE; k++){
                            if(devicespids[k] == chessboard[i][j]){

                                // L'ho trovato, non devo continuare a cercare
                                neighbours[k] = 1;                                
                                break;
                            }
                        }
                    } 
                } 
            }

            if(V(chessaccess) == -1){
                return -1;
            }
        }
    }

    return 0;

}

int insertElement(union acklist_union element){

    int index;

    if(P(chessaccess) == -1){
        return -1;
    }
    
    // Cerca la posizione dove deve essere scritto l'elemento aggiunto
    for(index = 0; index < ACKN && element.device_ack.message_id >= acklist[index][0].message_id && acklist[index][0].message_id != 0; index++);

    // Libero la posizione solo se serve
    if(acklist[index][0].message_id != 0){
        // Parte dal fondo e sposta avanti di uno tutti gli elementi fino alla posizione da riempire
        for(int i = ACKN-1; i >= index; i--){
            for(int j = 0; j < 6; j++){
                
                acklist[i][j] = acklist[i-1][j];

            }
        }
    }
    
    // Creo la nuova entry nella tabella
    acklist[index][0].message_id = element.device_ack.message_id;

    if(V(chessaccess) == -1){
        return -1;
    }

    return index;

}

void terminazionedevice(int sig){
    flag = 0;
}