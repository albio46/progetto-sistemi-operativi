/// @file shared_memory.c
/// @brief Contiene l'implementazione delle funzioni
///         specifiche per la gestione della MEMORIA CONDIVISA.

#include "shared_memory.h"

void *sharedmget(ssize_t size, char* msg){

    // Creo un nuovo id per la nuova shared memory
    int shmid = shmget(IPC_PRIVATE, size, IPC_CREAT | S_IRWXU);
    if (shmid == -1){
        ErrExit(msg);
    }

    // Attacco la shared memory allo spazio del programma chiamante
    void *pointer = shmat(shmid, NULL, 0);
    if (pointer == (void *)-1){
        ErrExit(msg);
    }


    // Segno la rimozione della shm quando sarà dellocata
    if(shmctl(shmid, IPC_RMID, NULL) == -1){
        ErrExit(msg);
    }

    // Ritorno il puntatore all'indirizzo a cui il segmento
    // è stato attaccato. Il chiamante si preoccuperà di
    // tipizzare il puntatore in base all'esigenza
    return pointer;
}