//Includes
#include "ack_manager.h"

//Global Variables
int firstFreePosition = 0;
Acknowledgment messagesToAck[ACKN][N_DEVICE];
int msg_queue_id = 0;


void ackManager (int server_msg_queue) {

    msg_queue_id = msgget(server_msg_queue, IPC_CREAT | S_IRUSR | S_IWUSR); //Open the message queue and get the ID

    do {

        if (flag == 0) break; //Check if it's a good day to die

        //Check and collect ACKs messages to send
        checkAckList();

        //We don't know how long will take the "sendAck" function. This way the delay will always be 5 sec (If everything goes right)
        alarm(5);

        //Send ACKs messages to client(s)
        sendAck();

        //Delay
        sleep(alarm(0));

    } while (1);

}

void checkAckList() { //Search into the shared memory to find messages received by all 5 devices

    //Block access to the shared memory
    P(acksemset);

    //Variables
    int i = 0, sum;

    while (acklist[i][0].message_id != 0 && i < ACKN) { //Search 'till reach the end or find an empty record

        sum = 0;

        for (int j = 0; j < N_DEVICE; j++){
            sum += (acklist[i][j+1].device_ack.message_id != 0) ? 1 : 0;
        }

        if (sum == N_DEVICE) { //Check if ALL the devices received the message.

            //If so, put the ACKs into the matrix messagesToAck to send them later
            for (int j = 0; j < N_DEVICE; j++){

                messagesToAck[firstFreePosition][j] = acklist[i][j + 1].device_ack;

            }

            firstFreePosition++; //<- Index for the matrix

            //Delete the record and refactor the shared memory.
            cleanAckList(i);
            i--;

        }

        i++;

    }

    //Free access to the shared memory
    V(acksemset);

}

void cleanAckList (int index) { //Move all the "after-index" record backwards.

    //Variables
    int i = index, j;

    while (acklist[i][0].message_id != 0 && i < ACKN) { //Search 'till the next empty record or the end of the shared memory

        for (j = 0; j < (N_DEVICE + 1); j++) acklist[i][j] = acklist[i+1][j]; //Move elements backwards

        i++;

    }

    //Set the last moved element as an empty record.
    acklist[i-1][0].message_id = 0;
    for (j = 1; j < N_DEVICE; j++) acklist[i-1][j].device_ack.message_id = 0;

}

void sendAck () { //Send ALL the ACKs messages to the clients

    //Variables
    int i = 0;

    while (i < firstFreePosition) {

        //Order the ACKs to send based on timestamp
        orderAckToSend(messagesToAck[i]);

        //Format the ACKs messages
        struct queued_message message;
        generateAcksMessage(messagesToAck[i], &message);

        //Send the ACKs message
        if (msgsnd(msg_queue_id, &message, sizeof(struct queued_message) - sizeof(long), 0) == -1) {

            printf("[" WARNING "WARNING" DEFAULT "] Failing sending devices' acks' message with id=%li to message queue.\n", message.mytype);

        }

        i++;

    }

    //Reset the firstFreePosition "index"
    firstFreePosition = 0;

}

void orderAckToSend (Acknowledgment ackToSend[N_DEVICE]) { //Order the ACKs to send based on the timestamp (Insertion Sort Style)

    //Variables
    int i, j;
    Acknowledgment key;

    for (i = 1; i < N_DEVICE; i++) { //DEVICE ACKs to sort

        j = i - 1;
        key = ackToSend[i];

        while (j >= 0 && ackToSend[j].timestamp > key.timestamp) { //Found a message sent after the key

            ackToSend[j + 1] = ackToSend[j]; //Move everything

            j = j - 1;

        }

        ackToSend[j + 1] = key;

    }

}

void generateAcksMessage (Acknowledgment acks[N_DEVICE], struct queued_message * message) { //Format the right way the ACKs messages

    //Set the property "mytype" as the message_id. It's the only way a client knows that the message is for him (Broadcast style)
    message->mytype = acks[0].message_id;

    for (int i = 0; i < N_DEVICE; i++) message->devices_acks[i] = acks[i];

}