/// @file shared_memory.h
/// @brief Contiene la definizioni di variabili e funzioni
///         specifiche per la gestione della MEMORIA CONDIVISA.

#pragma once

#include <sys/types.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <stdlib.h>

#include "defines.h"

// Dichiarazione shared memory
int (*chessboard)[10];
// TODO: Controllare che effettivamente funzioni bene
union acklist_union (*acklist)[N_DEVICE+1];
char *positionstring;
pid_t *devicespids;//[N_DEVICE];

/// @brief Crea una shared memory della dimensione specificata
///         se avviene un errore, stampa la stringa passata seguita dall'errore
void *sharedmget(ssize_t, char*);

