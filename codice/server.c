/// @file server.c
/// @brief Contiene l'implementazione del SERVER.

#include "server.h"

int flag = 1;

int main(int argc, char *argv[])
{

    // Controllo numero argomenti
    if (argc != 3)
    {
        printf("[" ERROR "ERROR" DEFAULT "]: " "Wrong argument number. Usage: \"./server msg_queue_key file_posizioni\"\n");
        exit(1);
    }

    // Serve per mandare il segnale!
    printf("[" WARNING "WARNING" DEFAULT "]: PID SERVER: %d\n", getpid());

    struct sembuf v[1] = {{1, 1, 0}};
    struct sembuf p[1] = {{0, -1, 0}};
    int pidackmanager, counter = 0;
    pid_t pid;

    // Inizializzazione shared memories
    chessboard = sharedmget(sizeof(int) * 100, "Chessboard shared memory");
    acklist = sharedmget(sizeof(union acklist_union) * ACKN * (N_DEVICE + 1), "Acknowledgment list shared memory");
    positionstring = sharedmget(sizeof(char) * ROWLEN, "Positions string shared memory");
    devicespids = sharedmget(sizeof(pid_t) * N_DEVICE, "Pid list shared memory");

    // Inizializzazione semafori
    acksemset = sem(IPC_PRIVATE, 1, "Acknowledgment semaphore creation");
    chessaccess = sem(IPC_PRIVATE, 1, "Chessboard semaphore creation");
    devices = sem(IPC_PRIVATE, N_DEVICE + 1, "Devices semaphore creation");

    // Impostazione valore iniziale semafori
    init(acksemset, 0, 1);
    init(chessaccess, 0, 1);
    semctl(devices, 0, SETALL, 0);

    /* *********** */
    /* Avvio figli */
    /* *********** */

    // Creazione Acknowledgment manager
    switch (pidackmanager = fork())
    {
    case -1:
        ErrExit("[" ERROR "ERROR" DEFAULT "]: Ack manager fork");
        break;

    case 0:
        ackManager(atoi(argv[1]));
        exit(0);
        break;

    }

    char err_string[60];

    for (int i = 0; i < N_DEVICE; i++)
    {
        switch (pid = fork())
        {
        case -1:
            ///strcat(err_string, itoa(i));
            //strcat(err_string, " ");

            sprintf(err_string, "%s %d %s", "[" ERROR "ERROR" DEFAULT "]: Device #", i, "start failed");

            ErrExit(err_string);
            break;

        case 0:
            device(i + 1);
            exit(0); 
            break;

        default:
            devicespids[i] = pid;
        }
    }


    // Esclusione segnali e registrazione handler SIGTERM
    // è dopo le fork per non influenzare i figli
    sigset_t all;
    sigfillset(&all);
    sigdelset(&all, SIGTERM);
    //sigdelset(&all, SIGALRM);
    sigdelset(&all, SIGUSR1);
    signal(SIGTERM, terminazione);
    signal(SIGALRM, terminazione);
    sigprocmask(SIG_BLOCK, &all, NULL);

    /* *************** */
    /* Setup movimento */
    /* *************** */

    // Apro il file con le posizioni
    int movementfd = open(argv[2], O_RDONLY);
    if (movementfd == -1)
    {
        ErrExit("[" ERROR "ERROR" DEFAULT "]: Movement file open error");
    }

    /* *************** */
    /* Ciclo movimento */
    /* *************** */
    do
    {

        // Preparo la stringa che i device andranno a leggere per muoversi
        if (read(movementfd, positionstring, ROWLEN) == -1)
        {
            ErrExit("[" ERROR "ERROR" DEFAULT "]: Row read");
        }

        // Timer del ciclo
        sleep(2);

        // Stampa righe, come da manuale
        printf("# Step %d: device positions ####################\n", counter + 1);

        // Segnale dell'inizio di movimento
        int result = semop(devices, v, 1);
        if (result == -1)
        {
            // Controllo se è un'errore perchè è richiesta la terminazione
            if (errno == EINTR || errno == EIDRM || errno == EINVAL)
            {
                break;
            }
            else
            {
                ErrExit("Device sem");
            }
        }

        // Timer di sicurezza
        alarm(5);

        // Aspetto che l'ultimo device mi liberi
        result = semop(devices, p, 1);
        if (result == -1)
        {
            // Controllo se è un'errore perchè è richiesta la terminazione
            if (errno == EINTR || errno == EIDRM  || errno == EINVAL)
            {
                break;
            }
            else
            {
                ErrExit("Device sem");
            }
        }

        // Resetto il timer di errore
        alarm(0);

        printf("################################################\n");

        // Preparo per il prossimo ciclo
        counter++;
        if (lseek(movementfd, ROWLEN * (counter % ROWN), SEEK_SET) == -1)
        {
            ErrExit("File index error");
        }

    } while (flag);

    // Segnalo ai figli di terminare
    for (int i = 0; i < N_DEVICE; i++)
    {
        kill(devicespids[i], SIGUSR2);
    }
    kill(pidackmanager, SIGUSR2);{
        if(errno == EEXIST){
        }
    }

    /* ************************* */
    /*    SEZIONE DI CLEANUP     */
    /* ************************* */

    // Detach della memoria condivisa
    if (shmdt(chessboard) == -1)
    {
        ErrExit("[" ERROR "ERROR" DEFAULT "]: Chess memory detach");
    }
    if (shmdt(acklist) == -1)
    {
        ErrExit("[" ERROR "ERROR" DEFAULT "]: Acknowledgment memory detach");
    }
    
    // Rimozione semafori
    if (semctl(chessaccess, 0, IPC_RMID, 0) == -1)
    {
        ErrExit("[" ERROR "ERROR" DEFAULT "]: Chess semaphores removal");
    }
    if (semctl(acksemset, 0, IPC_RMID, 0) == -1)
    {
        ErrExit("[" ERROR "ERROR" DEFAULT "]: Acknowledgment semaphore removal");
    }
    if (semctl(devices, 0, IPC_RMID, 0) == -1)
    {
        ErrExit("[" ERROR "ERROR" DEFAULT "]: Devices semaphore removal");
    }
    semget(SEMAPHORE_KEY, 1, IPC_CREAT | IPC_EXCL);
    if (semctl(semget(SEMAPHORE_KEY, 1, 0), 0, IPC_RMID, 0) == -1) ErrExit("[" ERROR "ERROR" DEFAULT "]: Clients semaphore removal");
    
    // Chiudo i file descriptor
    if (close(movementfd) == -1)
    {
        ErrExit("[" ERROR "ERROR" DEFAULT "]: Movement file close");
    }

    // Attendo che i figli terminino
    for (int i = 0; i < N_DEVICE; i++)
    {
    
        waitpid(devicespids[i], NULL, 0);
    }
    waitpid(pidackmanager, NULL, 0);

    return 0;
}


void terminazione(int sig)
{

    flag = 0;
}

int findAck(int message_id)
{

    int left = 0, right = 0, middle = 0, midid;

    // Attendo l'accesso alla shared memory
    if (P(acksemset) == -1)
    {
        return -1;
    }

    // Sposto l'estremo di destra all'ultima posizione occupata
    while (acklist[right][0].message_id != 0)
        right++;
    right--;

    // Controllo di non aver già trovato quello che mi serve
    if (acklist[left][0].message_id == message_id)
    {
        
        if (V(acksemset) == -1) return -1;
    
        return left;
    } else if (acklist[right][0].message_id == message_id)
    {
        
        if (V(acksemset) == -1) return -1;

        return right;
    }    

    // Comincio a cercare
    do
    {
        // Calcolare offset rispetto a left e riposizionare
        middle = left + ((right - left) / 2);
        midid = acklist[middle][0].message_id;
        if (message_id == midid)
        {
            if (V(acksemset) == -1) return -1;

            return middle;
        }

        (message_id > midid) ? (left = middle) : (right = middle);

    } while (right - left > 1);
    //while (middle != left && middle != right);

    // In teoria il primo non serve
    if (message_id == acklist[left][0].message_id)
    {
        if (V(acksemset) == -1) return -1;
        
        return left;
    } else if (message_id == acklist[right][0].message_id)
    {
        if (V(acksemset) == -1) return -1;
        
        return right;
    }

    // Libero la shared memory
    if (V(acksemset) == -1) return -1;

    return -ACKN;
}

int getReceivedarr(int row, int *acks)
{

    int count = 0;

    // Richiedo l'accesso alla shared memory
    if (P(acksemset) == -1)
    {
        return -1;
    }

    for (int i = 0; i < N_DEVICE; i++)
    {

        // Controlla che l'id nell'acknowledgment sia 0. Se non è 0,
        // l'ack è già stato ricevuto
        acks[i] = acklist[row][i + 1].device_ack.message_id == 0 ? 0 : 1;
        count += acks[i];
    }

    // Libero l'accesso alla shared memory
    if (V(acksemset) == -1)
    {
        return -1;
    }

    // Ritorno quanti hanno ricevuto il messaggio
    return count;
}