/// @file ack_manager.h
/// @brief Contiene la definizioni di variabili
///         e funzioni specifiche del gestore degli ack.

#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>

#include "err_exit.h"
#include "defines.h"
#include "shared_memory.h"
#include "semaphore.h"

extern int flag;

/// @brief "Main" function for the ack_manager. Infinite loop witch execute all the function with a delay of 5 sec.
///         Invoked by the server when ack_manager born.
///         Stopped (the loop) by the server when it's time to die
void ackManager (int server_msg_queue);

/// @brief Function that search in the shared memory "acklist" for messages received by all 5 devices.
///         If it find something it stores those "ACKs messages" into a matrix to send them later. (Outside the mutex)
///         and it delete them from the acklist
void checkAckList();

/// @brief Function that refactor the acklist after removing element overwriting them.
void cleanAckList (int index);

/// @brief Function that sort and send the ACKs messages to the clients (via message queue)
void sendAck ();

/// @brief Function that sort the ACKs messages with `Insertion Sort` algorithm
void orderAckToSend(Acknowledgment ackToSend[N_DEVICE]);

/// @brief Format the ACKs messages so they can be sent to the client
void generateAcksMessage(Acknowledgment acks[N_DEVICE], struct queued_message * message );