#include "defines.h"
#include <time.h>

#define TIMEDOUT 0

/// @brief Function to generate a "Message Object" aka set the Message struct
void generateMessageObject(Message *MESSAGE, pid_t pid_sender, pid_t pid_receiver, int message_id, const char *message, double max_distance);

/// @brief Function to convert a time_t variable into a yyyy-mm-dd hh:mm:ss date
char * getdate(time_t time, char *string);

/// @brief Function to handle SIGALRM signal
void semaphore_handler(int signal);

/// @brief Function to handle SIGALRM signal for message queue
void timeout_handler(int signal);

/// @brief Function to free all the allocated memory in case of exit for error
void garbageCollector(int fd, int messages_ids, char *output_text, char *line, char *date);