/// @file client.c
/// @brief Contiene l'implementazione del client.

//Includes
#include "defines.h"
#include "client.h"
#include "semaphore.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/msg.h>
#include <signal.h>
#include <sys/sem.h>
#include <asm/errno.h>
#include <math.h>

//Main
int main(int argc, char * argv[]) {

    if (argc < 2) { //Check if the user wrote the queue key

        printf("[" ERROR "ERROR" DEFAULT "] Message queue key not provided\n");

        exit(1);

    }

    //Initialize variables
    Message MESSAGE;
    key_t msg_queue_key = atoi(argv[1]);
    pid_t pid_device = -1;
    int message_id;
    char message[256];
    double max_distance = -1;
    char *fifo_path = "/tmp/dev_fifo.";
    char fifo_filename[26];
    int fd = -1;
    int messages_ids = 1;
    int check = 0;
    struct queued_message queuedMessage;
    char output_filename [20];
    char * output_text = malloc(sizeof(char) * 300);
    int client_semaphore;
    char *line = NULL;
    char c;
    char *date = NULL;

    signal(SIGALRM, semaphore_handler); //Set the handler for SIGALRM

    //Create or get the semaphore
    client_semaphore = semget(SEMAPHORE_KEY, 1, IPC_CREAT | IPC_EXCL | EEXIST | S_IRWXU | S_IRWXG | S_IRWXO);

    if (client_semaphore != -1) { //It's a brand new semaphore, let's initialize it

        semctl(client_semaphore, 0, SETVAL, 1);

    } else client_semaphore = semget(SEMAPHORE_KEY, 1, 0);

    printf(REQUEST "Please insert" DEFAULT " the pid of the device to send the message to.\n");
    do { //Check the pid given

        printf("device_pid :> ");

        check++;

        scanf("%d", &pid_device);

        //Generate the filename
        snprintf(fifo_filename, sizeof(fifo_filename), "%s%d", fifo_path, pid_device);

        //To chek if the pid given is correct, try to open the fifo of that pid
        fd = open(fifo_filename, O_RDONLY | O_NONBLOCK);
        if (fd != -1) break;

        printf("[" ERROR "ERROR" DEFAULT "] The pid given is wrong." REQUEST " Please retry\n" DEFAULT);

        if (check == ATTEMPTS) {

            printf("[" ERROR "ERROR" DEFAULT "] Too many attempts. Exiting...\n");

            garbageCollector(fd, messages_ids, output_text, line, date);

            exit(1);

        }

    } while (1);

    close(fd);
    fd = -1;
    check = 0;

    printf("[" WARNING "WARNING" DEFAULT "] Waiting the access to the messages_ids file...\n");
    alarm(TIMEOUT); //Set a timeout
    if (P(client_semaphore) == -1) garbageCollector(fd, messages_ids, output_text, line, date);

    alarm(0); //Delete the timeout
    messages_ids = open("messages_ids.txt", O_RDWR | O_CREAT | O_APPEND, S_IRWXU, S_IRWXG, S_IRWXO);

    if (messages_ids == -1) {

        printf("[" ERROR "ERROR" DEFAULT "] Error creating/opening the messages_ids txt file. Exiting...\n");

        garbageCollector(fd, messages_ids, output_text, line, date);

        exit(1);

    }

    printf(REQUEST "Please insert" DEFAULT " the id of the message.\n");
    do { //Check the id given

        line = malloc(sizeof(char) * 256);
        int occupied = 0, i = 0;

        printf("message_id :> ");

        check++;

        scanf("%i", &message_id);

        if (message_id >= 1) { //Check if already exist the same id into the file

            lseek(messages_ids, 0, SEEK_SET); //"Rewind" the stream

            while (read(messages_ids, &c, 1) != 0) {

                if (c == '\n') {

                    if (atoi(line) == message_id) {

                        occupied = 1;

                        break;

                    }

                    i = 0;

                } else {

                    line[i] = c;
                    i++;

                }

            }

            if (!occupied) {

                sprintf(line, "%i\n", message_id);

                lseek(messages_ids, -1, SEEK_END);

                write(messages_ids, line, strlen(line));

                sprintf(output_filename, "out_%i", message_id);

                break;

            } else printf("[" ERROR "ERROR" DEFAULT "] The id given is already in use." REQUEST " Please retry\n" DEFAULT);

        } else printf("[" ERROR "ERROR" DEFAULT "] The id given is wrong." REQUEST " Please retry\n" DEFAULT);

        if (check == ATTEMPTS) {

            printf("[" ERROR "ERROR" DEFAULT "] Too many attempts. Exiting...\n");

            garbageCollector(fd, messages_ids, output_text, line, date);

            exit(1);

        }

    } while (1);
    V(client_semaphore);
    check = 0;
    close(messages_ids);
    messages_ids = -1;

    printf(REQUEST "Please insert" DEFAULT " the message (max 256 char).\n[" WARNING "WARNING" DEFAULT "] Exceeding character will be truncated.\n");
    printf("message :>\n");
    getchar(); //Remove the '\n'
    fgets(message, sizeof(MESSAGE.message), stdin); //Get the string with spaces in secure way
    if ((strlen(message) > 0) && (message[strlen(message) - 1] == '\n')) message[strlen(message) - 1] = '\0'; //Remove exceeding things (Copy-Pasted from StackOverflow. Basically substitute the last "\n" (if exist) with "\0")

    printf(REQUEST "Please insert" DEFAULT " the maximum communication distance for the message.\n");
    do { //Check the distance given

        printf("max_distance :> ");

        check++;

        scanf("%lf", &max_distance);

        if (max_distance > 0 && max_distance < 10) break;

        printf("[" ERROR "ERROR" DEFAULT "] The distance given is wrong." REQUEST " Please retry\n" DEFAULT);

        if (check == ATTEMPTS) {

            printf("[" ERROR "ERROR" DEFAULT "] Too many attempts. Exiting...\n");

            garbageCollector(fd, messages_ids, output_text, line, date);

            exit(1);

        }

    } while (1);

    //Generate the Message "object"
    generateMessageObject(&MESSAGE, getpid(), pid_device, message_id, message, max_distance);

    //Write the message and check the outcome
    fd = open(fifo_filename, O_WRONLY | O_NONBLOCK);

    if (fd == -1) {

        printf("[" ERROR "ERROR" DEFAULT "] Error opening the device's fifo\n");

        garbageCollector(fd, messages_ids, output_text, line, date);

        exit(1);

    }

    int process = write(fd, &MESSAGE, sizeof(MESSAGE));

    if (process == -1) {

        printf("[" ERROR "ERROR" DEFAULT "] Error writing the message.\n");

        garbageCollector(fd, messages_ids, output_text, line, date);

        exit(1);

    } else printf("[" SUCCESS "SUCCESS" DEFAULT "] Successfully write the message into the device's pid.\n");

    //Free the memory
    close(fd);
    fd = -1;

    //Open the message queue.
    int message_queue_id = msgget(msg_queue_key, IPC_CREAT | S_IRUSR | S_IWUSR);

    //Wait for ACKs message
    signal(SIGALRM, timeout_handler); //Set the handler for SIGALRM
    alarm(30);
    if (msgrcv(message_queue_id, &queuedMessage, sizeof(struct queued_message) - sizeof(long), message_id, 0) == -1) {

        printf("[" ERROR "ERROR" DEFAULT "] Failed getting devices acks from message queue\n");

        garbageCollector(fd, messages_ids, output_text, line, date);

        exit(1);

    } else printf("[" SUCCESS "SUCCESS" DEFAULT "] Successfully obtained the devices acks from message queue with mytype: %li\n", queuedMessage.mytype);

    alarm(0);

    //Remove the message id from messages_ids
    P(client_semaphore);

    fd = open("messages_ids(temp).txt", O_WRONLY | O_CREAT | O_EXCL, S_IRWXU, S_IRWXG, S_IRWXO);

    messages_ids = open("messages_ids.txt", O_RDONLY);

    while (read(messages_ids, &c, 1) != 0) {

        int i = 0;

        if (c == '\n') {

            if (atoi(line) != message_id) {

                write(fd, line, strlen(line));

            }

            i = 0;

        } else {

            line[i] = c;
            i++;

        }

    }

    rename("messages_ids(temp).txt", "messages_ids.txt");

    V(client_semaphore);
    free(line);
    line = NULL;
    close(fd);
    fd = -1;
    close(messages_ids);
    messages_ids = -1;

    output_text = realloc(output_text, (sizeof(output_text) + (sizeof(queuedMessage.devices_acks)/sizeof(Acknowledgment) * 40)) * sizeof(char));

    //Convert the received ACKs message into the right output text
    sprintf(output_text, "Messaggio %li: %s\n", queuedMessage.mytype, message);

    for (int i = 0; i < (sizeof(queuedMessage.devices_acks)/sizeof(Acknowledgment)); i++) {

        char temp[42];

        date = getdate(queuedMessage.devices_acks[i].timestamp, date);
        snprintf(temp, sizeof(temp), "%i, %i, %s", queuedMessage.devices_acks[i].pid_sender, queuedMessage.devices_acks[i].pid_receiver, date);

        strcat(output_text, temp);

    }

    free(date);
    date = NULL;

    //Write the received ACKs message on file
    char * dummy = malloc(sizeof(char ) * (strlen(output_filename) + 4));
    sprintf(dummy, "%s.txt", output_filename);
    fd = open(dummy, O_WRONLY | O_CREAT | O_EXCL | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);
    char * new_filename;
    char temp[10];
    if (fd == -1) {

        new_filename = malloc(sizeof(char ) * (strlen(output_filename) + 8));
        int i = 1;

        while(fd == -1 && i <= 999) {

            strcpy(new_filename, output_filename);
            snprintf(temp, (strlen("().txt") + 1 + (size_t ) floor (log10 (abs (i)) + 1)), "(%i).txt", i);
            strcat(new_filename, temp);
            fd = open(new_filename, O_WRONLY | O_CREAT | O_EXCL | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);
            i ++;

        }

    }
    ssize_t numWrite = write(fd, output_text, strlen(output_text));

    if (numWrite != strlen(output_text)) {

        printf("[" ERROR "ERROR" DEFAULT "] Failed to write the output file\n");

        garbageCollector(fd, messages_ids, output_text, line, date);

        exit(1);

    } else {

        if (strlen(new_filename) <= 9) printf("[" SUCCESS "SUCCESS" DEFAULT "] Output file \"%s.txt\" wrote correctly.\n", output_filename);
        else printf("[" SUCCESS "SUCCESS" DEFAULT "] Output file \"%s\" wrote correctly.\n", new_filename);
    }

    close(fd);
    fd = -1;
    free(dummy);
    dummy = NULL;
    free(output_text);
    output_text = NULL;

    return 0;
}

void generateMessageObject (Message *MESSAGE, pid_t pid_sender, pid_t pid_receiver, int message_id, const char *message, double max_distance) { //Generate the Message "object"

    //Assign the elements
    MESSAGE->pid_sender = pid_sender;
    MESSAGE->pid_receiver = pid_receiver;
    MESSAGE->message_id = message_id;
    for (int i = 0; i < sizeof(MESSAGE->message) && message[i] != '\0'; i++) MESSAGE->message[i] = message[i];
    MESSAGE->max_distance = (int) max_distance;

}

char * getdate(time_t time, char * string){ //Convert a time_t variable into a "yyyy-mm-dd hh-mm-ss" date format

    string = malloc(22 * sizeof(char));
    struct tm * structtime = localtime(&time);

    sprintf(string, "%d-%d-%d %d:%d:%d\n",
            1900+structtime->tm_year,
            structtime->tm_mon,
            structtime->tm_mday,
            structtime->tm_hour,
            structtime->tm_min,
            structtime->tm_sec);

    return string;

}

void semaphore_handler(int signal) {

    if (signal == SIGALRM) {

        printf("[" ERROR "ERROR" DEFAULT "] Checking the messages_ids file is taking too long. Exiting...\n");

    }

}

void timeout_handler(int signal) {

    if (signal == SIGALRM) {

        printf("[" ERROR "ERROR" DEFAULT "] Waiting the message's acks is taking too long. Exiting...\n");

        exit(1);

    }

}

void garbageCollector(int fd, int messages_ids, char *output_text, char *line, char *date) {

    if (fd != -1) close(fd);

    if (messages_ids != -1) close(messages_ids);

    free(output_text);

    free(line);

    free(date);

}